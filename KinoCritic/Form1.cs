﻿using KinoCritic.InvisibleHeroes;
using KinoCritic.Makets;
using KinoCritic.Models;
using System;
using System.Threading;
using System.Windows.Forms;
using KinoCritic;
using System.Collections.Generic;

namespace KinoCritic
{
    public partial class Form1 : Form
    {
        public static string url;
        public static Movie previousMovie = null;
        public static Movie currentMovie = null;
        public Form1()
        {
            InitializeComponent();
            BossController controller = BossController.Instance;
            controller.Initialize(new MovieMockup(), new DownloadMockup(), new StartMockup(), this);
            searchText.KeyDown += searchText_KeyDown;
        }

        private void searchText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BossController controller = BossController.Instance;
                url = searchText.Text;
                timer1.Enabled = true;
                Thread thread = new Thread(FillMovie);
                controller.RenderDownloadForm();
                thread.Start();

                searchText.Text = string.Empty;                
            }
        }

        private static void FillMovie()
        {

            IMDb.MoviePage api = new IMDb.MoviePage();
            api.Url = url;
            Movie movie = new Movie();
            movie = new Movie();
            movie.name = api.Name;
            movie.yearTitle = api.Year;
            movie.genre = api.Genre;

            movie.image = ImageMaster.ConvertUrlToImage(api.Image);
            var stars = api.Stars;
            movie.actor1 = new KinoPerson(stars[0]);
            movie.actor2 = new KinoPerson(stars[1]);
            movie.director = new KinoPerson(api.Director);

            var reviews = api.Reviews;

            ReviewJuicerTalker rjt = new ReviewJuicerTalker();
            rjt.MakeMachineLearning(movie, reviews);

            var actorRatings = rjt.actorRatings;
            var directorRating = rjt.directorR;
            var plotRating = rjt.plot;

            KinoPerson plotKr = new KinoPerson(plotRating.title, plotRating.val, plotRating.text);
            KinoPerson dirKr = new KinoPerson(movie.director.name, directorRating.val, directorRating.text);
            KinoPerson actor1Kr = new KinoPerson(movie.actor1.name, actorRatings[0].val, actorRatings[0].text);
            KinoPerson actor2Kr = new KinoPerson(movie.actor2.name, actorRatings[1].val, actorRatings[1].text);


            movie.actor1 = actor1Kr;
            movie.actor2 = actor2Kr;
            movie.director = dirKr;
            movie.plot = plotKr;

            currentMovie = movie;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (previousMovie != currentMovie)
            {
                BossController controller = BossController.Instance;
                controller.RenderMoviePage(currentMovie);
                previousMovie = currentMovie;
            }
        }

        private void searchText_Enter(object sender, EventArgs e)
        {
            searchLabel.Hide();
        }

        private void searchText_Leave(object sender, EventArgs e)
        {
            if (searchLabel.Text == string.Empty)
            {
                searchLabel.Show();
            }
        }
    }
}
