﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinoCritic.Makets;
using System.Windows.Forms;
using KinoCritic.Models;

namespace KinoCritic
{
    public class BossController
    {
        private MovieMockup movieMockup;
        private DownloadMockup downloadMockup;
        private StartMockup startMockup;

        private Mockup currentMockup;

        private Form form;

        private static BossController instance;

        private BossController() { }
        
        public void Initialize(MovieMockup movieMockup, DownloadMockup downloadMockup, StartMockup startMockup, Form form)
        {
            this.movieMockup = movieMockup;
            this.downloadMockup = downloadMockup;
            this.startMockup = startMockup;
            this.form = form;

            movieMockup.Initialize();
            downloadMockup.Initialize();
            startMockup.Initialize();

            downloadMockup.Controls.ForEach((x) => 
            {
                form.Controls.Add(x);
                x.BringToFront();
            });

            movieMockup.Controls.ForEach((x) => 
            {
                form.Controls.Add(x);
                x.SendToBack();
            });

            startMockup.Controls.ForEach((x) =>
            {
                form.Controls.Add(x);
            });

            currentMockup = startMockup;

            downloadMockup.Hide();
            movieMockup.Hide();
        }

        public void RenderMoviePage(Movie movie)
        {
            currentMockup.Hide();
            movieMockup.FillContent(movie);
            currentMockup = movieMockup;
            currentMockup.Show();
        }

        public void RenderDownloadForm()
        {
            currentMockup.HideSpecialFields();
            
            currentMockup = downloadMockup;
            downloadMockup.Show();
        }

        //BossController - singletone class
        public static BossController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BossController();
                }
                return instance;
            }
        }
    }
}
