﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinoCritic.Models
{
    public class KinoPerson
    {
        public string name;
        public string position;
        public string description;
        public double value;

        public KinoPerson() { }

        public KinoPerson(string name)
        {
            this.name = name;
        }

        public KinoPerson(string name, double value, string description)
        {
            this.name = name;
            this.description = description;
            this.value = value;
        }
    }
}
