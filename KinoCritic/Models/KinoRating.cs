﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinoCritic.Models
{
    public class KinoRating
    {
        public string title;
        public double value;
        public string text;

        public KinoRating() { }

        public KinoRating(string title, double value, string text)
        {
            this.title = title;
            this.value = value;
            this.text = text;
        }
    }
}