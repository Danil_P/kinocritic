﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinoCritic.Models
{
    public class Movie
    {
        public string name;
        public string yearTitle;
        public Bitmap image;
        public KinoPerson director;
        public KinoPerson actor1, actor2;
        public KinoPerson plot;

        public List<KinoRating> ratings = new List<KinoRating>();
        public string genre;

        public Movie() { }

        public Movie(string name, string yearTitle, Bitmap image)
        {
            this.name = name;
            this.yearTitle = yearTitle;
            this.image = image;
        }

        public Movie(string name, string yearTitle, Bitmap image, string genre)
        {
            this.name = name;
            this.yearTitle = yearTitle;
            this.image = image;
            this.genre = genre;
        }
    }
}
