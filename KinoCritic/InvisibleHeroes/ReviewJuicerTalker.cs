﻿using KinoCritic.Models;
using KinoCritic.RatingSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinoCritic.InvisibleHeroes
{
    public class ReviewJuicerTalker
    {


        public List<Rating> actorRatings = new List<Rating>();
        public Rating plot;
        public Rating directorR;

        public void MakeMachineLearning(Movie movie, List<string> strReviews)
        {
            var name1 = movie.actor1.name.Split(' ').ToList();
            var ac1Name = name1[0];
            var ac1Surname = name1[name1.Count - 1];

            var name2 = movie.actor2.name.Split(' ').ToList();
            var ac2Name = name2[0];
            var ac2Surname = name2[name2.Count - 1];

            var actor1 = new Person(ac1Name, ac1Surname);
            var actor2 = new Person(ac2Name, ac2Surname);
            var personList = new List<Person>();

            personList.Add(actor1);
            personList.Add(actor2);

            
            var name3 = movie.director.name.Split(' ').ToList();
            var dirName = name3[0];
            var dirSurname = name3[name3.Count - 1];

            var director = new Person(dirName, dirSurname);

            ReviewJuicer rj = new ReviewJuicer(strReviews, personList, director);
            rj.ProcessReviews();
            foreach (var rating in rj.GetMovieRatings())
            {
                rating.ProcessRating();
                if (rating.GetType() == typeof(ActorRating))
                {
                    ActorRating ar = rating as ActorRating;
                    actorRatings.Add(ar);
                }
                if (rating.GetType() == typeof(DirectorRating))
                {
                    DirectorRating dr = rating as DirectorRating;
                    directorR = dr;
                }
                if (rating.GetType() == typeof(PlotRating))
                {
                    PlotRating pr = rating as PlotRating;
                    plot = pr;
                }
            }
        }
    }
}
