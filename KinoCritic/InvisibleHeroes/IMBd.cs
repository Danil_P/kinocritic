﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using System.Net;
using System.Collections;
using System.Web;
using System.Drawing;
using KinoCritic.Models;

namespace KinoCritic.InvisibleHeroes
{
    /// <summary>
    /// IMDb API
    /// </summary>
    public class IMDb
    {
        /// <summary>
        /// Contains all options for the movie page
        /// </summary>
        public class MoviePage
        {
            /// <summary>
            /// Contains the Html code of the selected url
            /// </summary>
            public string HtmlCode;
            public string htmlCastCode = null;
            private string _url;
            private readonly HtmlDocument _htmlMainPage = new HtmlDocument();
            private string HtmlDownloader
            {
                get
                {
                    using (WebClient client = new WebClient())
                    {
                        return client.DownloadString(_url);
                    }
                }
            }

            /// <summary>
            /// The Url from the IMDb Movie Page
            /// </summary>
            public string Url
            {
                get
                {
                    return _url;
                }
                set
                {
                    _url = value.TrimEnd('/');
                    HtmlCode = HtmlDownloader;
                    _htmlMainPage.LoadHtml(HtmlCode);
                }
            }

            public List<string> Reviews
            {
                get
                {
                    var ratingList = new List<string>();
                    for (int i = 0; i < 40; i += 10)
                    {
                        string newUrl = ReviewUrl(i);
                        using (WebClient client = new WebClient())
                        {
                            var htmlReviewCode = client.DownloadString(newUrl);
                            HtmlDocument htmlReviewPage = new HtmlDocument();
                            htmlReviewPage.LoadHtml(htmlReviewCode);
                            newUrl.ToString();
                            HtmlNodeCollection nodes = htmlReviewPage.DocumentNode.SelectNodes("//*[@id='tn15content']/p");

                            if (nodes.Count < 3)
                            {
                                break;
                            }

                            foreach (var node in nodes)
                            {
                                if (node.InnerText != "Add another review")
                                    ratingList.Add(node.InnerText.Trim());
                            }
                        }
                    }
                    return ratingList;
                }
            }

            public string Image
            {
                get
                {
                    string newUrl = ReviewUrl();
                    using (WebClient client = new WebClient())
                    {
                        var htmlReviewCode = client.DownloadString(newUrl);
                        HtmlDocument htmlReviewPage = new HtmlDocument();
                        htmlReviewPage.LoadHtml(htmlReviewCode);
                        newUrl.ToString();
                        var node = htmlReviewPage.DocumentNode
                            .SelectSingleNode("//*[@id='tn15lhs']/div[1]/a/img")
                            .GetAttributeValue("src", " ");

                        return node;
                    }
                }
            }


            private string ReviewUrl(int id = 0)
            {
                return $"{_url}/reviews?start={id}";
            }

            private string CastUrl
            {
                get
                {
                    return $"{_url}/fullcredits";
                }
            }

            /// <summary>
            /// Gets the name of the movie
            /// </summary>
            public string Name
            {
                get
                {
                    //*[@id="overview-top"]/h1/text()
                    //*[@id="overview-top"]/h1/span[1]
                    return _htmlMainPage.DocumentNode
                        .SelectSingleNode("//*[@id='ratingWidget']/p[1]/strong")
                        .InnerText
                        .Trim();
                }
            }

            /// <summary>
            /// Gets the release year from the movie
            /// </summary>
            public string Year
            {
                get
                {
                    var yearNode = _htmlMainPage.DocumentNode.SelectSingleNode("//*[@id='titleYear']/a");
                    if(yearNode != null)
                    {
                        return yearNode.InnerHtml.Trim();
                    }
                    return "";
                }
            }

            /// <summary>
            /// Gets the genre(s) from the movie
            /// </summary>
            public string Genre
            {
                get
                {
                    //*[@id="overview-top"]/div[2]/a[1]
                    //*[@id="overview-top"]/div[2]
                    var genreNode = _htmlMainPage.DocumentNode.SelectSingleNode("//*[@id='titleStoryLine']/div[4]/a[1]");
                    if (genreNode != null)
                    {
                        return genreNode.InnerHtml.Trim();
                    }
                    return "";

                }
            }

            /// <summary>
            /// Gets the stars that are on the movie page
            /// </summary>
            public List<String> Stars
            {
                get
                {
                    using (WebClient client = new WebClient())
                    {
                        if (htmlCastCode == null)
                        {
                            htmlCastCode = client.DownloadString(CastUrl);
                        }
                        HtmlDocument htmlReviewPage = new HtmlDocument();
                        htmlReviewPage.LoadHtml(htmlCastCode);
                        var list = new List<String>();
                        var firstStar = htmlReviewPage.DocumentNode
                            .SelectSingleNode("//*[@id='fullcredits_content']/table[3]/tr[2]/td[2]")
                            .InnerText
                            .Trim();
                        var secondStar = htmlReviewPage.DocumentNode
                            .SelectSingleNode("//*[@id='fullcredits_content']/table[3]/tr[3]/td[2]")
                            .InnerText
                            .Trim();
                        list.Add(firstStar);
                        list.Add(secondStar);
                        return list;
                    }
                }
            }
            /// <summary>
            /// Gets the director(s) of the movie
            /// </summary>
            public string Director
            {
                get
                {
                    using (WebClient client = new WebClient())
                    {
                        if (htmlCastCode == null)
                        {
                            htmlCastCode = client.DownloadString(CastUrl);
                        }
                        HtmlDocument htmlReviewPage = new HtmlDocument();
                        htmlReviewPage.LoadHtml(htmlCastCode);
                        return htmlReviewPage.DocumentNode
                            .SelectSingleNode("//*[@id='fullcredits_content']/table[1]/tbody/tr/td[1]/a")
                            .InnerText
                            .Trim();
                    }
                }
            }
        }
    }
}