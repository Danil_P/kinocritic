﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Drawing;

namespace KinoCritic.InvisibleHeroes
{
    public class KinoParser
    {

        public string id;

        private string SiteUrl
        {
            get
            {
                return $"https://plus.kinopoisk.ru/film/{id}/";
            }
        }

        private const string image = "https://st.kp.yandex.net/images/film_big/";

        public KinoParser(string id)
        {
            this.id = id;
        }

        public Bitmap GetImage()
        {
            string imageLink = $"{image}{id}.jpg";
            return ImageMaster.ConvertUrlToImage(imageLink);
        }

        public string GetName()
        {
            HtmlDocument htmlDocument = new HtmlDocument();
            var web = new HtmlWeb
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.UTF8,
            };

            htmlDocument = web.Load(/*SiteUrl*/"http://www.imdb.com/title/tt1022603/?ref_=nv_sr_1");


            List <string> a = new List<string>();

            HtmlNodeCollection NoAltElements = htmlDocument
                .DocumentNode
                .SelectNodes(".//div");
            // проверка на наличие найденных узлов
            if (NoAltElements != null)
            {
                foreach (HtmlNode HN in NoAltElements)
                {
                    a.Add(HN.InnerText);
                }
            }

            var r = a;
                return "";
        }

        /*
        public string GetImage(string id)
        {
            string link = GetImageLink(id);


            HtmlDocument htmlDocument = new HtmlDocument();
            var web = new HtmlWeb
            {
                AutoDetectEncoding = false,
                OverrideEncoding = Encoding.UTF8,
            };

            try
            {
                htmlDocument = web.Load(htmlRef);
            }
            catch
            {
                return "";
            }
            HtmlNode NoAltElements = htmlDocument.DocumentNode.SelectSingleNode("//*[@id='wrap']/a");





            return NoAltElements.ToString();
        }
        */
    }
}
