﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KinoCritic.InvisibleHeroes
{
    public class ImageMaster
    {
        public static Bitmap ConvertUrlToImage(string url)
        {
            using (WebClient webClient = new WebClient())
            {
                byte[] data = webClient.DownloadData(url);

                using (MemoryStream mem = new MemoryStream(data))
                {
                    Image revievedImage = Image.FromStream(mem);
                    return new Bitmap(revievedImage);
                }
            }
        }
    }
}
