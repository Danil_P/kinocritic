﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KinoCritic.Makets
{
    public class LoadingPanel : TransparentPanel
    {
        public Label label;
        public PictureBox downloadImage;

        private int centerHor(int elementWidth)
        {
            return (Width - elementWidth) / 2;
        }

        public LoadingPanel()
        {
            Size = new Size(860, 601);
            this.Opacity = 70;
            downloadImage = new PictureBox();
            downloadImage.Image = Image.FromFile("downloading.gif");
            downloadImage.BackColor = Color.Transparent;
            downloadImage.Size = new Size(340, 340);
            downloadImage.Location = new Point(centerHor(downloadImage.Width), 86);
            downloadImage.SizeMode = PictureBoxSizeMode.StretchImage;
            Controls.Add(downloadImage);

            label = new Label();
            label.Location = new Point(0, 463);
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Font = new Font("Helvetica", 30, FontStyle.Regular, GraphicsUnit.Pixel);
            label.Width = Width;
            label.Height = 40;
            SetLabelText("");
            Controls.Add(label);
        }

        public void SetLabelText(string text)
        {
            label.Text = text;
            label.Location = new Point(centerHor(label.Width), 463);
        }
    }
}
