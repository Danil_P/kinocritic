﻿using KinoCritic.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KinoCritic.Makets
{
    class RatingLink : Label
    {
        public KinoPerson rating;
        public ReviewBox reviewBox;

        public RatingLink() { }

        public RatingLink(KinoPerson rating, ReviewBox reviewBox)
        {
            this.reviewBox = reviewBox;
            this.rating = rating;
            Width = 196;
            Height = 41;
            BackColor = Color.FromArgb(118, 169, 255);
            ForeColor = Color.White;
            TextAlign = ContentAlignment.MiddleCenter;
            Font = new Font("Roboto Light", 26, FontStyle.Regular, GraphicsUnit.Pixel);

            Text = rating.description;

            Click += RatingLink_Click;
        }

        private void RatingLink_Click(object sender, EventArgs e)
        {
            var ratingNum = this.rating.value;
            if (this.rating.value == double.NaN)
            {
                reviewBox.title.Text = this.rating.name;
            } else
            {
                reviewBox.title.Text = this.rating.name + " " + ratingNum.ToString();
            }
            
            reviewBox.review.Text = this.rating.description;
        }
    }
}
