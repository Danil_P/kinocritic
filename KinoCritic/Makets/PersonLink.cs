﻿using KinoCritic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KinoCritic.Makets
{
    public class PersonLink : Label
    {
        private KinoPerson person;
        public KinoPerson Person {
            get { return person;  }
            set
            {
                person = value;
                this.Text = person.name;
            }
        }

        public string description;
        private ReviewBox reviewBox;

        public PersonLink() { }

        public PersonLink(KinoPerson person, ReviewBox reviewBox)
        {
            this.person = person;
            this.reviewBox = reviewBox;
            Text = person.name;
            ForeColor = System.Drawing.Color.FromArgb(41, 121, 255);
            Click += PersonLink_Click;
            MouseEnter += PersonLink_MouseEnter;
            MouseLeave += PersonLink_MouseLeave;
        }

        private void PersonLink_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.AppStarting;
        }

        private void PersonLink_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void PersonLink_Click(object sender, EventArgs e)
        {

            var ratingNum = person.value;
            if (person.value.ToString() == "не число")
            {
                reviewBox.title.Text = person.name;
            }
            else
            {
                reviewBox.title.Text = person.name + " " + person.value.ToString();
            }


            //reviewBox.title.Text = person.name + person.value.ToString();
            reviewBox.review.Text = person.description;
        }
    }
}
