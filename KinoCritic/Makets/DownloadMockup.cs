﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KinoCritic.Makets
{
    public class DownloadMockup : Mockup
    {
        private readonly string[] infoTexts = {
            "оцениваем плотность распределения классов",
            "покупаем попкорн",
            "забираем 'Оскар' у Ди Каприо",
            "взламываем Пентагон",
            "плачем от Титаника",
            "вычисляем линейный дискриминант Фишера",
            "смотрим Аватара в 1D",
            "ждём Хатико",
            "О боже мой, мы убили Кенни!",
            "освобождаем Джанго",
            "ждём секунду на планете Миллера",
            "убегаем от Тома",
            "ловим Джерри",
            "ведём диалоги с Тарантино",
            "крестим отца",
            "трём волшебную лампу",
            "чиним Звезду Смерти",
            "кусаем паука"
        };
        public LoadingPanel loadingPanel;
        private Timer labelChanger;
        private int currentLabelIndex = 0;

        public override void Initialize()
        {
            loadingPanel = new LoadingPanel();
            labelChanger = new Timer();

            labelChanger = new Timer();
            labelChanger.Tick += LabelChanger_Tick;
            labelChanger.Interval = 2000;
            labelChanger.Enabled = true;
            labelChanger.Start();

            Controls.Add(loadingPanel);
        }

        private void LabelChanger_Tick(object sender, EventArgs e)
        {
            currentLabelIndex = (currentLabelIndex + 1) % infoTexts.Count();
            loadingPanel.SetLabelText(infoTexts[currentLabelIndex]);
        }
    }
}