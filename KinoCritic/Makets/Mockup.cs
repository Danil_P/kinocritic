﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KinoCritic.Makets
{
    public abstract class Mockup
    {
        public List<Control> Controls { get; set; } = new List<Control>();

        public List<Control> SpecialControls { get; set; } = new List<Control>();

        public Mockup() { }

        public abstract void Initialize();

        public void Hide()
        {
            Controls.ForEach((control) => { control.Hide(); });
        }

        public void Show()
        {
            Controls.ForEach((control) => { control.Show(); });
        }

        public void HideSpecialFields()
        {
            SpecialControls.ForEach((control) => { control.Hide(); });
        }

        public void ShowSpecialFields()
        {
            SpecialControls.ForEach((control) => { control.Show(); });
        }
    }
}
