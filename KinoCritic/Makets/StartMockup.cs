﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KinoCritic.Makets
{
    public class StartMockup : Mockup
    {
        private Label movieLabelPart1;
        private Label movieLabelPart2;
        private PictureBox searchTip;

        public override void Initialize()
        {
            movieLabelPart1 = new Label();
            movieLabelPart1.Location = new Point(225, 259);
            movieLabelPart1.Font = new Font("Roboto", 72, FontStyle.Bold, GraphicsUnit.Pixel);
            movieLabelPart1.AutoSize = true;
            movieLabelPart1.Text = "Kino";
            movieLabelPart1.ForeColor = Color.Red;
            Controls.Add(movieLabelPart1);
            SpecialControls.Add(movieLabelPart1);

            movieLabelPart2 = new Label();
            movieLabelPart2.Location = new Point(390, 259);
            movieLabelPart2.Font = new Font("Roboto", 72, FontStyle.Bold, GraphicsUnit.Pixel);
            movieLabelPart2.AutoSize = true;
            movieLabelPart2.Text = "Critic";
            movieLabelPart2.ForeColor = Color.Orange;
            Controls.Add(movieLabelPart2);
            SpecialControls.Add(movieLabelPart2);

            searchTip = new PictureBox();
            searchTip.Image = Image.FromFile("searchTip.png");
            searchTip.SizeMode = PictureBoxSizeMode.StretchImage;
            searchTip.Size = new Size(170, 150);
            searchTip.Location = new Point(10, 35);
            Controls.Add(searchTip);
            SpecialControls.Add(searchTip);
        }
    }
}