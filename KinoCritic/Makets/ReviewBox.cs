﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KinoCritic.Makets
{
    public class ReviewBox : Panel
    {
        public Label title, review;
        public Color color;

        public ReviewBox(string title, string review, Color color)
        {
            this.title = new Label();
            this.review = new Label();
            this.title.Text = title;
            this.review.Text = review;

            this.title.Location = new Point(3, 4);
            this.review.Location = new Point(3, 52);

            this.title.AutoSize = true;
            this.review.AutoSize = true;

            this.title.Width = Width;
            this.review.Width = Width;

            this.title.Font = new Font("Roboto", 30, FontStyle.Bold, GraphicsUnit.Pixel);
            this.title.ForeColor = color;
            this.review.Font = new Font("Roboto", 20, FontStyle.Regular, GraphicsUnit.Pixel);
            this.review.ForeColor = color;

            this.Controls.Add(this.title);
            this.Controls.Add(this.review);
        }
    }
}
