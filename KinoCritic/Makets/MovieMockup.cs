﻿using KinoCritic.InvisibleHeroes;
using KinoCritic.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KinoCritic.Makets
{
    public class MovieMockup : Mockup
    {
        private PictureBox poster;
        private Label title, genreYearTitle, directorTitle, actorsTitle, verdictTitle;
        private ReviewBox reviewBox;
        private List<RatingLink> ratingLinks = new List<RatingLink>();
        private PersonLink directorLink, actor1Link, actor2Link;
        private RatingLink param1, param2, param3;

        public override void Initialize()
        {
            //review box
            reviewBox = new ReviewBox("Игра актёров", text, Color.FromArgb(0, 114, 54));
            reviewBox.Location = new Point(12, 340);
            reviewBox.Size = new Size(840, 200);
            Controls.Add(reviewBox);
            SpecialControls.Add(reviewBox);

            poster = new PictureBox();
            poster.Image = new Bitmap("poster.jpg");            //Destroy
            poster.SizeMode = PictureBoxSizeMode.StretchImage;
            poster.Size = new Size(196, 275);
            poster.Location = new Point(12, 60);
            Controls.Add(poster);

            title = new Label();
            title.Location = new Point(220, 60);
            title.Font = new Font("Roboto", 28, FontStyle.Bold, GraphicsUnit.Pixel);
            title.AutoSize = true;
            Controls.Add(title);

            genreYearTitle = new Label();
            genreYearTitle.Location = new Point(220, 93);
            genreYearTitle.Font = new Font("Roboto", 20, FontStyle.Regular, GraphicsUnit.Pixel);
            genreYearTitle.ForeColor = Color.FromArgb(142, 156, 164);
            genreYearTitle.AutoSize = true;
            Controls.Add(genreYearTitle);

            directorTitle = new Label();
            directorTitle.Text = "Director:";
            directorTitle.Location = new Point(220, 157);
            directorTitle.Font = new Font("Roboto", 24, FontStyle.Regular, GraphicsUnit.Pixel);
            directorTitle.ForeColor = Color.FromArgb(84, 110, 122);
            Controls.Add(directorTitle);

            actorsTitle = new Label();
            actorsTitle.Text = "Stars:";
            actorsTitle.Location = new Point(220, 184);
            actorsTitle.Font = new Font("Roboto", 24, FontStyle.Regular, GraphicsUnit.Pixel);
            actorsTitle.ForeColor = Color.FromArgb(84, 110, 122);
            Controls.Add(actorsTitle);

            directorLink = new PersonLink(new KinoPerson(), reviewBox);
            directorLink.Location = new Point(330, 157);
            directorLink.Font = new Font("Roboto", 24, FontStyle.Regular, GraphicsUnit.Pixel);
            directorLink.AutoSize = true;
            Controls.Add(directorLink);

            actor1Link = new PersonLink(new KinoPerson(), reviewBox);
            actor1Link.Location = new Point(330, 184);
            actor1Link.Font = new Font("Roboto", 24, FontStyle.Regular, GraphicsUnit.Pixel);
            actor1Link.AutoSize = true;
            Controls.Add(actor1Link);

            actor2Link = new PersonLink(new KinoPerson(), reviewBox);
            actor2Link.Location = new Point(330, 208);
            actor2Link.Font = new Font("Roboto", 24, FontStyle.Regular, GraphicsUnit.Pixel);
            actor2Link.AutoSize = true;
            Controls.Add(actor2Link);

            verdictTitle = new Label();
            verdictTitle.Text = "Result";
            verdictTitle.Location = new Point(220, 253);
            verdictTitle.Font = new Font("Robota", 28, FontStyle.Regular, GraphicsUnit.Pixel);
            verdictTitle.ForeColor = Color.FromArgb(84, 110, 122);
            verdictTitle.AutoSize = true;
            Controls.Add(verdictTitle);

            param1 = new RatingLink(new KinoPerson(), reviewBox);
            param1.Text = "Plot";
            param1.Location = new Point(213, 293);
            Controls.Add(param1);
        }

        public void FillContent(Movie movie)
        {
            poster.Image = movie.image;
            title.Text = movie.name;
            genreYearTitle.Text = movie.genre;
            genreYearTitle.Text += $" ({movie.yearTitle})";
            directorLink.Person = movie.director;
            actor1Link.Person = movie.actor1;
            actor2Link.Person = movie.actor2;
            param1.rating = new KinoPerson(movie.plot.name, 3.2, movie.plot.description);

            foreach (var rating in movie.ratings)
            {
                verdictTitle.Text = rating.text;
            }
        }
        /*
        private void AddDynamicControls(List<Person> persons)
        {
            int j = 0;
            for (int i = 0; i < persons.Count; i++)
            {
                PersonLink personLink = new PersonLink(persons[i]);
                Point pos;
                if (i == 0)
                {
                    pos = new Point(actorsTitle.Location.X + actorsTitle.Width,
                        actorsTitle.Location.Y);
                } else
                {
                    pos = new Point(personLinks[i - 1].Location.X + personLinks[i - 1].Width,
                        actorsTitle.Location.Y);
                }
                personLink.Location = pos;
                personLink.Font = new Font("Helvetica", 19, FontStyle.Regular, GraphicsUnit.Pixel);
                personLink.ForeColor = Color.FromArgb(74, 74, 219);
                if (personLinks.Count >= i)
                {
                    personLinks.Add(personLink);
                } else
                {
                    personLinks[i].Person = persons[i];
                }
                j++;
                Controls.Add(personLink);
            }

            for (int i = j; i < personLinks.Count; i++)
            {
                personLinks[i].Hide();
            }
        }
        */
        public string text = @"Проблемы ребят осложняются тем, что под кайфом 
они малофункциональны… а под кайфом они — всегда. 
Единым источником проблем и кайфа выступает  
«Ананасовый экспресс» — забористый продукт скрещивания 
«Голубой устрицы», «Афган - куша», «Чокнутого северного сияния»
и «Суперкрасной отмороженной снежинки». 
Впрочем, Провидение, традиционно доброе к младенцам и пьяницам,
наших героев даже не ведет за руку, а буквально тащит за шиворот,
наделяя неуязвимостью терминаторов";
    }
}
